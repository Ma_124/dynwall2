// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DYN_WALL2_ICONSMODEL_H
#define DYN_WALL2_ICONSMODEL_H

#include <QObject>

class IconDataObject : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString x READ x NOTIFY xChanged)
    Q_PROPERTY(QString y READ y NOTIFY yChanged)
    Q_PROPERTY(QString w READ w NOTIFY wChanged)
    Q_PROPERTY(QString h READ h NOTIFY hChanged)
    Q_PROPERTY(QString id READ id NOTIFY idChanged)
    Q_PROPERTY(QString img READ img NOTIFY imgChanged)
    Q_PROPERTY(QString action READ action NOTIFY actionChanged)

public:
    explicit IconDataObject(QObject* parent = nullptr,
            const QString& x = nullptr,
            const QString& y = nullptr,
            const QString& w = nullptr,
            const QString& h = nullptr,
            const QString& id = nullptr,
            const QString& img = nullptr,
            const QString& action = nullptr);
    ~IconDataObject() override;

    QString x();
    QString y();
    QString w();
    QString h();
    QString id();
    QString img();
    QString action();

Q_SIGNALS:
    void xChanged(QString);
    void yChanged(QString);
    void wChanged(QString);
    void hChanged(QString);
    void idChanged(QString);
    void imgChanged(QString);
    void actionChanged(QString);

protected:
    QString m_x;
    QString m_y;
    QString m_w;
    QString m_h;
    QString m_id;
    QString m_img;
    QString m_action;
};

#endif //DYN_WALL2_ICONSMODEL_H
