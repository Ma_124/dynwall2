// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <utility>

#include "IconDataObject.h"

IconDataObject::IconDataObject(QObject* parent,
                               const QString& x,
                               const QString& y,
                               const QString& w,
                               const QString& h,
                               const QString& id,
                               const QString& img,
                               const QString& action)
        : QObject(parent)
{
    m_x = x;
    emit xChanged(x);
    m_y = y;
    emit yChanged(y);
    m_w = w;
    emit wChanged(w);
    m_h = h;
    emit hChanged(h);
    m_id = id;
    emit idChanged(id);
    m_img = img;
    emit imgChanged(img);
    m_action = action;
    emit actionChanged(action);
}

IconDataObject::~IconDataObject()
= default;

QString IconDataObject::x() {
    return m_x;
}

QString IconDataObject::y() {
    return m_y;
}

QString IconDataObject::w() {
    return m_w;
}

QString IconDataObject::h() {
    return m_h;
}

QString IconDataObject::id() {
    return m_id;
}

QString IconDataObject::img() {
    return m_img;
}

QString IconDataObject::action() {
    return m_action;
}
