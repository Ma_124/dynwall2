// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <utility>

#include "DynamicWallpaper.h"
#include <iostream>
#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>

DynamicWallpaper::DynamicWallpaper(QObject* parent)
        : QObject(parent)
{
    m_config = QString(std::getenv("DYNWALL_CONFIG"));
    if (m_config.length() == 0)
        m_config = QString(std::getenv("HOME")) + "/.dynwall/dynwall.json";
    reloadConfig(m_config);
}

DynamicWallpaper::~DynamicWallpaper()
= default;

QString DynamicWallpaper::qtVersion() {
    return QString(qVersion());
}

void DynamicWallpaper::startProcessDetached(const QString &cmd) {
    QProcess::startDetached(cmd);
}

void DynamicWallpaper::setWallpaper(const QString& wallpaper) {
    m_wallpaper = wallpaper;
    emit wallpaperChanged(wallpaper);
}

QString DynamicWallpaper::wallpaper() {
    return m_wallpaper;
}

void DynamicWallpaper::setIcons(QList<QObject *> list) {
    m_icons = std::move(list);
    emit iconsChanged(m_icons);
}

QList<QObject*> DynamicWallpaper::readIcons() {
    return m_icons;
}

void DynamicWallpaper::doAction(const QString &action) {
    using namespace std;

    QStringList argv = QString(action).replace("\u001B", "").replace("\\;", "\u001B").split(";");
    argv.replaceInStrings("\u001B", ";");
    if (argv[0] == "") {
        return;
    } else if (argv[0] == "cmd") {
        if (argv.length() != 2) {
            cout << "[DynWall 2] action: cmd: wrong number of arguments: expected: 1; got: " << to_string(argv.length()-1) << endl;
            return;
        }
        startProcessDetached(argv[1]);
    } else if (argv[0] == "launch") {
        if (argv.length() != 2) {
            cout << "[DynWall 2] action: launch: wrong number of arguments: expected: 1; got: "
                 << to_string(argv.length() - 1) << endl;
            return;
        }
        QProcess::startDetached("gtk-launch", {argv[1]});
    } else if (argv[0] == "sh") {
        if (argv.length() != 2) {
            cout << "[DynWall 2] action: sh: wrong number of arguments: expected: 1; got: "
                 << to_string(argv.length() - 1) << endl;
            return;
        }
        QProcess::startDetached("sh", {"-c", argv[1]});
    } else if (argv[0] == "open") {
        if (argv.length() != 2) {
            cout << "[DynWall 2] action: open: wrong number of arguments: expected: 1; got: "
                 << to_string(argv.length() - 1) << endl;
            return;
        }
        QProcess::startDetached("xdg-open", {argv[1]});
    } else if (argv[0] == "cfg") {
        if (argv.length() > 2) {
            cout << "[DynWall 2] action: cfg: wrong number of arguments: expected: 1; got: "
                 << to_string(argv.length() - 1) << endl;
            return;
        }
        reloadConfig(argv[1]);
    } else if (argv[0] == "launch-file") {
        if (argv.length() != 2) {
            cout << "[DynWall 2] action: launch-file: wrong number of arguments: expected: 1; got: "
                 << to_string(argv.length() - 1) << endl;
            return;
        }
        // Workaround for bug in xdg-open
        QProcess::startDetached("sh", {"-c", QString::fromStdString("`grep '^Exec' ") + argv[1] + " | tail -1 | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^\"//g' | sed 's/\" *$//g'` &"});
    } else {
        cout << "[DynWall 2] action: unknown: '" << argv[0].toStdString() << "'" << endl;
    }
}

void DynamicWallpaper::reloadConfig(const QString& cfgf) {
    using namespace std;
    using namespace rapidjson;

    cout << "[DynWall 2] config: reloading: " << cfgf.toStdString() << endl;

    std::ifstream ifs { cfgf.toStdString() };
    if (!ifs.is_open()) {
        cout << "[DynWall 2] config: could not open config file" << endl;
        return;
    }

    IStreamWrapper isw { ifs };

    Document doc;
    if (doc.ParseStream(isw).HasParseError()) {
        cout << "[DynWall 2] config: Error  : " << doc.GetParseError()  << '\n'
             << "                    Offset : " << doc.GetErrorOffset() << endl;
        return;
    }

    if (!doc.HasMember("wallpaper")) {
        cout << "[DynWall 2] config: missing property 'wallpaper'" << endl;
    } else {
        if (!doc["wallpaper"].IsString()) {
            cout << "[DynWall 2] config: 'wallpaper' must be a string" << endl;
        } else {
            setWallpaper(doc["wallpaper"].GetString());
        }
    }

    if (doc.HasMember("icons")) {
        if (!doc["icons"].IsArray()) {
            cout << "[DynWall 2] config: 'icons' must be an array" << endl;
        } else {
            QList<QObject *> list;

            for (rapidjson::SizeType i = 0; i < doc["icons"].GetArray().Size(); i++) {
                if (!doc["icons"][i].IsObject()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "]' must be an object" << endl;
                    continue;
                }
                if (!doc["icons"][i].HasMember("x")) {
                    cout << "[DynWall 2] config: missing property 'icons[" << to_string(i) << "].x'" << endl;
                    continue;
                }
                if (!doc["icons"][i].HasMember("y")) {
                    cout << "[DynWall 2] config: missing property 'icons[" << to_string(i) << "].y'" << endl;
                    continue;
                }
                if (!doc["icons"][i].HasMember("w")) {
                    cout << "[DynWall 2] config: missing property 'icons[" << to_string(i) << "].w'" << endl;
                    continue;
                }
                if (!doc["icons"][i].HasMember("h")) {
                    cout << "[DynWall 2] config: missing property 'icons[" << to_string(i) << "].h'" << endl;
                    continue;
                }
                if (!doc["icons"][i].HasMember("img")) {
                    cout << "[DynWall 2] config: missing property 'icons[" << to_string(i) << "].img'" << endl;
                    continue;
                }

                if (!doc["icons"][i]["x"].IsNumber()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "].x' must be a number" << endl;
                    continue;
                }
                if (!doc["icons"][i]["y"].IsNumber()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "].y' must be a number" << endl;
                    continue;
                }
                if (!doc["icons"][i]["w"].IsNumber()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "].w' must be a number" << endl;
                    continue;
                }
                if (!doc["icons"][i]["h"].IsNumber()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "].h' must be a number" << endl;
                    continue;
                }
                if (!doc["icons"][i]["img"].IsString()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "].img' must be a string" << endl;
                    continue;
                }
                if (doc["icons"][i].HasMember("action") && !doc["icons"][i]["action"].IsString()) {
                    cout << "[DynWall 2] config: 'icons[" << to_string(i) << "].action' must be a string" << endl;
                    continue;
                }

                list.append(new IconDataObject(nullptr,
                                               QString::number(doc["icons"][i]["x"].GetInt()),
                                               QString::number(doc["icons"][i]["y"].GetInt()),
                                               QString::number(doc["icons"][i]["w"].GetInt()),
                                               QString::number(doc["icons"][i]["h"].GetInt()),
                                               doc["icons"][i]["id"].GetString(),
                                               doc["icons"][i]["img"].GetString(),
                                               doc["icons"][i].HasMember("action") ? doc["icons"][i]["action"].GetString() : ""));
            }

            setIcons(list);
        }
    }

    cout << "[DynWall 2] config: reloaded" << endl;
}
