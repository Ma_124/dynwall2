// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DYN_WALL_DYNAMICWALLPAPER_H
#define DYN_WALL_DYNAMICWALLPAPER_H

#include <QImage>
#include <QString>
#include <QProcess>
#include <stdlib.h>
#include <QQmlEngine>
#include "IconDataObject.h"

class DynamicWallpaper : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString qtVersion READ qtVersion)
    Q_PROPERTY(QList<QObject*> icons READ readIcons WRITE setIcons NOTIFY iconsChanged)
    Q_PROPERTY(QString wallpaper READ wallpaper WRITE setWallpaper NOTIFY wallpaperChanged)

public:
    explicit DynamicWallpaper(QObject* parent = nullptr);
    ~DynamicWallpaper() override;

    Q_INVOKABLE void reloadConfig(const QString& cfg);
    Q_INVOKABLE void doAction(const QString& action);

    QString qtVersion();
    static void startProcessDetached(const QString& cmd);

    QList<QObject*> readIcons();
    void setIcons(QList<QObject*>);

    void setWallpaper(const QString&);
    QString wallpaper();

Q_SIGNALS:
    void iconsChanged(QList<QObject*> icons);
    void wallpaperChanged(QString wallpaper);

private:
    Q_DISABLE_COPY(DynamicWallpaper)
    QList<QObject*> m_icons;
    QString m_wallpaper;
    QString m_config;
};

#endif //DYN_WALL_DYNAMICWALLPAPER_H
