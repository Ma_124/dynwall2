// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "plugin.h"
#include "DynamicWallpaper.h"
#include <QQmlEngine>
#include <QQmlContext>

void Plugin::registerTypes(const char* uri)
{
    Q_ASSERT(uri == QLatin1String("com.gitlab.ma124.internal.dynwall2"));
    qmlRegisterType<DynamicWallpaper>(uri, 1, 0, "DynamicWallpaper");
}
