// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import QtQuick 2.9
import org.kde.plasma.core 2.0 as PlasmaCore

import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import com.gitlab.ma124.internal.dynwall2 1.0

Item {
    DynamicWallpaper {
        id: dynwall
    }

    Image {
        anchors.fill: parent
        source: dynwall.wallpaper
    }

    Repeater {
        id: icons
        anchors.fill: parent

        model: dynwall.icons
        Image {
            x: modelData.x
            y: modelData.y
            width: modelData.w
            height: modelData.h
            source: modelData.img
            opacity: 0.8 // TODO put in config (opacity)
            mipmap: true
            smooth: true
            // TODO visibility groups (itemAt); Impl: get index by id in bin/dynwall and put it in the IconDataObject and emit event that toggles visibility

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    console.log("[DynWall 2] ui: action: " + modelData.action)
                    dynwall.doAction(modelData.action)
                }
                onEntered: {
                    parent.opacity = 1.0 // TODO put in config (hover_opacity)
                }
                onExited: {
                    parent.opacity = 0.8 // TODO put in config (opacity)
                }
            }

            Behavior on opacity {
                PropertyAnimation {
                    easing.type: Easing.InOutQuad // TODO put in config (hover_opacity_easing_curve)
                    duration: 75 // TODO put in config (hover_opacity_easing_duration)
                }
            }
        }
    }
}
