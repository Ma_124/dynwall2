# Dynwall 2
A dynamic desktop for KDE Plasma.

## Installation
### From Source
```bash
$ git clone "https://gitlab.com/Ma_124/dynwall2"
$ cd dynwall2
$ ./install.sh
```

## Configuration
Create a [YAML](https://yaml.org/ ".yml or .yaml") file at the recommended path `~/.dynwall/dynwall.yml` in your favourite editor.

It supports the following keys natively:
```yaml
# Set a wallpaper.
wallpaper: /path/to/wallpaper.jpeg

# Manually position icons.
icons:
  - # An unique ID
    id: manual
    # The (x/y) coordinates. (0/0) is at the top left corner of the screen.
    x: 0
    y: 0
    # The width and height of your image
    w: 64
    h: 64
    # A path to the icon
    img: /path/to/icon.png
    # The action to perform if this icon is clicked.
    # These are explained in the Actions section.
    action: cfg;/home/<your user>/.dynwall/dynwall.yml

# vim: et:sw=2:ts=2:
```

But as you can see this is quite labour intensive. Instead you can write python scripts that do this work for you. They are called "filters".
For this next example we will use the [`hex-icons` filter](https://gitlab.com/Ma_124/dynwall2-filters/-/blob/master/hex-icons.py) to produce the following desktop:
**image TBA**

```yaml
# Setting a wallpaper still works the same
wallpaper: /path/to/wallpaper.jpeg

# This tells DynWall2 where to find the filters. It's just a list of URLs pointing to a python script.
# When they are used the first time they are downloaded to ~/.dynwall/plugins/<ID> where <ID> is a long hexadecimal string.
filters:
  - "https://gitlab.com/Ma_124/dynwall2-filters/raw/master/hex-icons.py"

# This is the new key introduced and interpreted by the hex-icons filter.
hex-icons:
  - # This defines a hex icon which launches Dolphin.
    # These coordinates are no longer in a cartesian system but instead specify the position in the hexagonal grid, eg.:
    # (0/0) (1/0)
    #   (0/1)
    # (0/2) (1/2)
    x: 0
    y: 0
    # These support the same values as the normal icons
    img: "/path/to/icons/folder.png"
    action: "cmd;/usr/bin/dolphin"
  - # Create a second icon next to the previous one
    x: 1
    y: 0
    img: "/path/to/icons/terminal.png"
    action: "cmd;/usr/bin/konsole"

# vim: et:sw=2:ts=2:
```

After you've written either style of config run:
```bash
$ dynwall compile ~/.dynwall/dynwall.yml ~/.dynwall/dynwall.json
```

This will apply all the filters and create a machine readable (JSON) file. If you want to load it from a different location set `$DYNWALL_CONFIG`.

## Actions
Actions always have a name and a list of arguments separated by semicola (`;` U+003B).
For example: `sh;echo Hello World` where `sh` is the command and `echo Hello World` the first argument.

Command  | Arguments    | Description                                                                                   | Example
---------|--------------|-----------------------------------------------------------------------------------------------|---------
`cmd`    | `<path>`     | Calls the file at `<path>`. You cannot pass any additional arguments.                         | `cmd;/usr/bin/konsole`
`sh`     | `<pipeline>` | Calls `sh -c '<pipeline>'`.                                                                   | `sh;/usr/bin/konsole -e /bin/tmux`
`launch` | `<name>`     | Calls `gtk-launch <name>`. `<name>` should be a file name in `/usr/share/applications/`.      | `launch;org.kde.konsole.desktop`
`open`   | `<path>`     | Calls `xdg-open <path>` which opens the file in the associated program.                       | `open;/home/ma/Documents/finance.docx`
`cfg`    | `[<path>]`   | Loads the *JSON* config at `<path>` or the initial one if it is called without any arguments. | `cfg;/home/ma/.dynwall/games.json`
<!-- launch-open is a bit hacky and therefor not included in the docs -->

## Usage
*Before enabling DynWall make sure you followed the steps in the [Configuration](#configuration) section.*

First you'll need to enable DynWall2 by going in the KDE desktop configuration by right clicking on your desktop and choosing "Configure Desktop":

![Right click and choose "Configure Desktop"](https://gitlab.com/Ma_124/pages-static-shared/-/raw/master/kde_configure_desktop.png)

Then change your Layout to Desktop and choose "DynWall 2" from the Wallpaper Type dropdown.

![Right click and choose "Configure Desktop"](https://gitlab.com/Ma_124/pages-static-shared/-/raw/master/kde_desktop_settings_dynwall.png)

### Troubleshooting
#### Wallpaper is just black.
QT does not support the image format you are using. Try converting it.

#### Restart Plasmashell and see logs
All of DynWalls log entries start with `[DynWall 2]`.

```bash
$ sh -c "kquitapp5 plasmashell && kstart5 plasmashell"
```

#### Uninstall
```bash
$ sudo rm -rf /usr/share/plasma/wallpapers/com.gitlab.ma124.dynwall2 \
              /usr/share/metainfo/com.gitlab.ma124.dynwall2.appdata.xml \
              /usr/share/kservices5/plasma-wallpaper-com.gitlab.ma124.dynwall2.desktop \
              /usr/lib/x86_64-linux-gnu/qt5/qml/com/gitlab/ma124/internal/dynwall2
```

## License
Copyright 2019 Ma_124, [ma124.js.org](https://ma124.js.org) <ma_124+oss@pm.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

